CREATE TABLE vz_fire_station (
  id              VARCHAR2(40)	 NOT NULL,
  address         VARCHAR2(254)  NOT NULL,
  longitude       VARCHAR2(254)  NOT NULL,
  latitude        VARCHAR2(254)  NOT NULL,
  phone           VARCHAR2(254)  NOT NULL,
  email           VARCHAR2(254)  NOT NULL,
  type            INTEGER        NULL,
  CONSTRAINT vz_fire_station_p PRIMARY KEY(id)
);

CREATE TABLE vz_autos (
  id              VARCHAR2(40)   NOT NULL REFERENCES vz_fire_station(id),
  auto_id         VARCHAR2(40)   NOT NULL REFERENCES auto(id),
  CONSTRAINT vz_autos_p PRIMARY KEY(id,auto_id)
);

CREATE TABLE vz_district_station (
  district_id     VARCHAR2(254)  NOT NULL REFERENCES district(id),
  fire_station_id VARCHAR2(254)  NOT NULL REFERENCES fire_station(id),
  CONSTRAINT vz_district_station_p PRIMARY KEY(district_id, fire_station_id)
);

CREATE TABLE vz_district (
  id             VARCHAR2(254)  NOT NULL,
  population     INTEGER        NOT NULL,
  name           VARCHAR2(254)  NOT NULL,
  CONSTRAINT vz_district_p PRIMARY KEY(id)
);