drop table vz_auto;

CREATE TABLE vz_auto(
  id         VARCHAR2(40)  NOT NULL,
  number     VARCHAR2(40)  NOT NULL,
  type       INTEGER       NOT NULL,
  CONSTRAINT auto_p PRIMARY KEY(id)
);

CREATE TABLE vz_history(
  id         VARCHAR2(254) NOT NULL REFERENCES vz_auto(id),
  time_start TIMESTAMP     NOT NULL,
  time_end   TIMESTAMP     NOT NULL,
  who        VARCHAR2(40)  NOT NULL,
  CONSTRAINT vz_history PRIMARY KEY(id)
);